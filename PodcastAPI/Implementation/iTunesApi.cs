﻿namespace PodcastAPI.Implementation
{
    using System;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;
    using System.Xml;
    using DTO;
    using PodcastAPI.Interface;
    using System.Xml.Serialization;

    public class iTunesApi : IPodcastApi
    {
        public async Task<PodcastGetDto> Get(string rssUrl)
        {
            if (String.IsNullOrWhiteSpace(rssUrl))
            {
                throw new ArgumentException($"The {nameof(rssUrl)} parameter is null or empty");
            }
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await httpClient.GetAsync(rssUrl);
                
                if (response.IsSuccessStatusCode)
                {
                    PodcastGetDto result;
                    using (Stream content = await response.Content.ReadAsStreamAsync())
                    {
                        using (XmlReader xmlReader = XmlReader.Create(content))
                        {
                            xmlReader.ReadToDescendant("channel");
                            XmlSerializer serializer = new XmlSerializer(typeof(PodcastGetDto), new XmlRootAttribute("channel"));
                            result = (PodcastGetDto)serializer.Deserialize(xmlReader.ReadSubtree());
                        }
                    }
                    return result;

                }
            }
            return null;
        }

        public async Task<PodcastSearchDto> Search(string searchQuery)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri("https://itunes.apple.com/");
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await httpClient.GetAsync($"search?media=podcast&country=US&term={searchQuery}");

                if (response.IsSuccessStatusCode)
                {

                    var content = await response.Content.ReadAsStringAsync();
                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject<PodcastSearchDto>(content);
                    return result;
                }

            }
            return null;
        }
    }

}
