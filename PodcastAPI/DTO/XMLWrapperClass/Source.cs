﻿namespace PodcastAPI.DTO.XMLWrapperClass
{
    using System.Xml.Serialization;

    public class Source
    {
        [XmlAttribute("url")]
        public string MediaUrl { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }
    }
}
