﻿namespace PodcastAPI.DTO.XMLWrapperClass
{
    using System.Xml.Serialization;
    
    public class Cover
    {
        [XmlAttribute("href")]
        public string Url { get; set; }
    }
}
