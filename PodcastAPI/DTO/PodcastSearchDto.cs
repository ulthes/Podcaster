﻿namespace PodcastAPI.DTO
{
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    [JsonObject(MemberSerialization.OptIn)]
    public class PodcastSearchDto
    {
        [JsonProperty("resultCount")]
        public int ResultCount { get; set; }

        [JsonProperty("results")]
        public IEnumerable<PodcastSearchResultDto> Results { get; set; }
    }
}