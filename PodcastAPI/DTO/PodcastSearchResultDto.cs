﻿namespace PodcastAPI.DTO
{
    using Newtonsoft.Json;

    [JsonObject(MemberSerialization.OptIn)]
    public class PodcastSearchResultDto
    {
        [JsonProperty("artistName")]
        public string Author { get; set; }

        [JsonProperty("collectionName")]
        public string Name { get; set; }

        [JsonProperty("feedUrl")]
        public string FeedUrl { get; set; }

        [JsonProperty("artworkUrl600")]
        public string Cover { get; set; }

        [JsonProperty("trackCount")]
        public int TrackCount { get; set; }
    }
}
