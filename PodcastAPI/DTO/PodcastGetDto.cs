﻿namespace PodcastAPI.DTO
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    [XmlRoot("channel"), XmlType("channel")]
    public class PodcastGetDto
    {
        [XmlElement("title")]
        public string Title { get; set; }

        [XmlElement("link")]
        public string Link { get; set; }

        [XmlElement("copyright")]
        public string Copyright { get; set; }

        [XmlElement("description")]
        public string Description { get; set; }

        [XmlElement(ElementName = "image", Namespace = "http://www.itunes.com/dtds/podcast-1.0.dtd")]
        public XMLWrapperClass.Cover Cover { get; set; }

        [XmlElement("item")]
        public List<PodcastGetResultDto> Items { get; set; }
    }
}
