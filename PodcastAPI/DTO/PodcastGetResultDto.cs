﻿namespace PodcastAPI.DTO
{
    using System;
    using System.Globalization;
    using System.Xml.Serialization;

    public class PodcastGetResultDto
    {
        private readonly string[] _formats = { @"h\:mm\:ss\.FFF", @"mm\:ss\.FFF", @"h\:mm\:ss", @"mm\:ss"};

        [XmlElement("title")]
        public string Title { get; set; }

        [XmlElement("link")]
        public string Link { get; set; }

        [XmlElement(ElementName = "pubDate")]
        public string PublicationDateString { get; set; }

        [XmlIgnore]
        public DateTime PublicationDate => DateTime.Parse(PublicationDateString);

        [XmlElement("description")]
        public string Description { get; set; }

        [XmlElement("enclosure")]
        public XMLWrapperClass.Source Source { get; set; }
        
        [XmlElement("guid")]
        public int Id { get; set; }

        [XmlElement(ElementName = "duration", Namespace = "http://www.itunes.com/dtds/podcast-1.0.dtd")]
        public string DurationString { get; set; }

        [XmlIgnore]
        public TimeSpan Duration => TimeSpan.ParseExact(DurationString, _formats, CultureInfo.InvariantCulture);

        [XmlElement(ElementName = "keywords", Namespace = "http://www.itunes.com/dtds/podcast-1.0.dtd")]
        public string Keywords { get; set; }
    }
}
