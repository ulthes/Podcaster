﻿namespace PodcastAPI.Interface
{
    using System.Threading.Tasks;

    public interface IPodcastApi
    {
        Task<DTO.PodcastSearchDto> Search(string searchQuery);

        Task<DTO.PodcastGetDto> Get(string rssUrl);
    }
}
