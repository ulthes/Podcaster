﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Podcaster.Controllers
{
    using System.Runtime.InteropServices.ComTypes;
    using PodcastAPI.Implementation;
    using PodcastAPI.Interface;
    using ViewModels;

    public class HomeController : Controller
    {
        private IPodcastApi _podcastApi;

        public HomeController()
        {
            _podcastApi = new iTunesApi();
        }

        public IActionResult Index()
        {
           return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public async Task<IActionResult> PodcastDetails(string urlToPodcast)
        {
            var result = await _podcastApi.Get(urlToPodcast);
            var viewModel = new PodcastDetailsViewModel()
            {
                Copyright = result.Copyright,
                Cover = result.Cover.Url,
                Description = result.Description,
                Link = result.Link,
                Title = result.Title,
                Items = result.Items.Select(x => new PodcastDetailsItemViewModel()
                {
                    Description = x.Description,
                    Duration = x.Duration,
                    Id = x.Id,
                    Keywords = x.Keywords,
                    Link = x.Link,
                    PublicationDate = x.PublicationDate,
                    Source = x.Source.MediaUrl,
                    Title = x.Title
                }).ToList()
            };

            return View("PodcastDetails", viewModel);
        }

        public IActionResult Error()
        {
            return View();
        }

        [HttpGet("{searchPodcast}")]
        public async Task<IActionResult> Search(string searchQuery)
        {
            string uriSearchQuery = searchQuery.Replace(' ', '+');
            var searchResult = await _podcastApi.Search(uriSearchQuery);

            ViewModels.PodcastsListViewModel results = new PodcastsListViewModel()
            {
                ResultCount = searchResult.ResultCount,
                Results = searchResult.Results.Select(x => new PodcastViewModel()
                {
                    Name = x.Name,
                    Author = x.Author,
                    Cover = x.Cover,
                    TrackCount = x.TrackCount,
                    FeedUrl = x.FeedUrl
                }).ToList()
            };

            return PartialView("_PodcastSearchResultPartial",results);
        }
    }
}
