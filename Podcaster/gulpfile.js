/// <binding ProjectOpened='build-and-watch' />
/*
This file is the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. https://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp');
var sass = require('gulp-sass');
var sassSourcemaps = require('gulp-sourcemaps');


var PATHS = {
    SASS: {
        MAIN: 'Styles/site.scss',
        INCLUDE_PATH: 'Styles/modules/**/*.scss',
    },
    CSS: 'wwwroot/css/'
};

gulp.task('sass', function () {
    return gulp.src(PATHS.SASS.MAIN)
        .pipe(sass({
            includePaths: PATHS.SASS.INCLUDE_PATH,
            errLogToConsole: true,
            indentedSyntax: false,
            outputStyle: 'expanded'
        }))
        .pipe(sassSourcemaps.init())
        .pipe(sassSourcemaps.write('./'))
        .pipe(gulp.dest(PATHS.CSS));
});

gulp.task('watch', function(){
    gulp.watch([PATHS.SASS.MAIN, PATHS.SASS.INCLUDE_PATH], ['sass']);
});

gulp.task('build-and-watch', ['sass', 'watch']);