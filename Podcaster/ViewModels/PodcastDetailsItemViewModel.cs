﻿namespace Podcaster.ViewModels
{
    using System;
    using System.Globalization;
    using System.Xml.Serialization;

    public class PodcastDetailsItemViewModel
    {
        public string Title { get; set; }

        public string Link { get; set; }

        public string PublicationDateString { get; set; }

        public DateTime PublicationDate { get; set; }

        public string Description { get; set; }

        public string Source { get; set; }
        
        public int Id { get; set; }

        public string DurationString { get; set; }

        public TimeSpan Duration { get; set; }

        public string Keywords { get; set; }
    }
}
