﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Podcaster.ViewModels
{
    public class PodcastViewModel
    {
        public string Author { get; set; }

        public string Name { get; set; }

        public string FeedUrl { get; set; }

        public string Cover { get; set; }

        public int TrackCount { get; set; }

    }
}
