﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Podcaster.ViewModels
{
    public class PodcastDetailsViewModel
    {
        public string Title { get; set; }

        public string Link { get; set; }

        public string Copyright { get; set; }

        public string Description { get; set; }

        public string Cover { get; set; }

        public List<PodcastDetailsItemViewModel> Items { get; set; }
    }
}
