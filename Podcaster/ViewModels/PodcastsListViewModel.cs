﻿namespace Podcaster.ViewModels
{
    using System.Collections.Generic;

    public class PodcastsListViewModel
    {
        public int ResultCount { get; set; }

        public IEnumerable<PodcastViewModel> Results { get; set; }
    }
}