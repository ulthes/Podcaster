# Podcaster

# PROJECT IS ON HOLD.

## Prerequisities

This project required .Net Core 1.1. 
If you have installed .Net Core in version 2.x then you need to install version 1.1 additionaly, since .Net Core 2.x is not compatible with previous verions.

## What this project is

This project is a something to spend free time on. 

Plans are to provide a static search and play podcasts with an POSSIBLE option to save list of podcast to a cookies.

## What this project is not

It's not another product for viewing and managing podcasts with account registering etc.

This is just a hobby project, but I don't have any plans to host it with a full Db.

If there's going to be Db involved it will still be available locally, via host's machine.

Unfortunately or fortunately Gitlab does not allow hosting dynamic pages.
