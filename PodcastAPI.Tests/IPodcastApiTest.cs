﻿namespace PodcastAPI.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Castle.Core.Internal;
    using DTO;
    using Interface;
    using Xunit;
    using Moq;

    public class IPodcastApiTest
    {
        private readonly Mock<IPodcastApi> _podcast;

        public IPodcastApiTest()
        {
            _podcast = new Mock<IPodcastApi>();
        }

        [Fact]
        public async Task EmptySearchReturnsEmptyList()
        {

            _podcast.Setup(x => x.Search(string.Empty)).ReturnsAsync(new PodcastSearchDto()
            {
                Results = new List<PodcastSearchResultDto>(),
                ResultCount = 0
            });
            var responseFromApi = await _podcast.Object.Search(string.Empty);
            Assert.True(responseFromApi.ResultCount == 0 && !responseFromApi.Results.Any());
        }

        [Fact]
        public async Task NonEmptySearchReturnsList()
        {
            string searchedQuery = "rooster";
            _podcast.Setup(x => x.Search(searchedQuery)).ReturnsAsync(new PodcastSearchDto()
            {
                Results = new List<PodcastSearchResultDto>()
                {
                    new PodcastSearchResultDto()
                },
                ResultCount = 1
            });
            var responseFromApi = await _podcast.Object.Search(searchedQuery);
            Assert.True(responseFromApi.ResultCount > 0 && responseFromApi.Results.Any());
        }

        [Fact]
        public async Task EmptyGetThrowsException()
        {
            _podcast.Setup(x => x.Get(string.Empty)).ThrowsAsync(new ArgumentException("No Podcast rss link provided"));

            await Assert.ThrowsAsync<ArgumentException>(async () => await _podcast.Object.Get(string.Empty));
        }

        [Fact]
        public async Task NonEmptyGetReturnsPodcastDetails()
        {
            string getQuery = "1d1ft3Q";

            _podcast.Setup(x => x.Get(getQuery)).ReturnsAsync(new PodcastGetDto()
            {
                Items = new List<PodcastGetResultDto>(),
                Title = "Some Title"
            });

            var responseFromApi = await _podcast.Object.Get(getQuery);

            Assert.False(responseFromApi.Title.IsNullOrEmpty() && !responseFromApi.Items.Any());
        }
    }
}
